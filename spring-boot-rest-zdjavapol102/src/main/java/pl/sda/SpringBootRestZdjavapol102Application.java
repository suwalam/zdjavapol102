package pl.sda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRestZdjavapol102Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRestZdjavapol102Application.class, args);
	}

}
