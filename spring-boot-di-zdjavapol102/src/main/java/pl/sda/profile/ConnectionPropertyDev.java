package pl.sda.profile;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile("dev")
@Component
public class ConnectionPropertyDev implements ConnectionProperty {

    @Override
    public String getConnection() {
        return "connection DEV";
    }
}
