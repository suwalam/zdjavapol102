package pl.sda.dao.impl;

import pl.sda.dao.PersonDao;
import pl.sda.model.Person;

import java.util.Arrays;
import java.util.List;

public class PersonDaoMockImpl implements PersonDao {

    private List<Person> personList;

    public PersonDaoMockImpl() {
        personList = Arrays.asList(
                new Person(1, "Michał", "Nowak"),
                new Person(2, "Anna", "Nowak"),
                new Person(3, "Jan", "Kowalski")
        );
    }

    @Override
    public Person getById(Integer id) {
        for (Person person : personList) {
            if (person.getId().equals(id)) {
                return person;
            }
        }
        throw new IllegalArgumentException("Unrecognized id " + id);
    }

    @Override
    public List<Person> getAll() {
        return personList;
    }
}
