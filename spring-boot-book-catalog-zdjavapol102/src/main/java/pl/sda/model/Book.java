package pl.sda.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@Entity
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Size(min = 3, max=100, message = "Title size must be at least 3 characters and max 100 characters")
    private String title;

    @Size(min = 3, max=100, message = "Author size must be at least 3 characters and max 100 characters")
    private String author;

    @Size(min = 13, max=13, message = "ISBN size must be 13 characters")
    private String isbn;

    @Column(length = 1000)
    private String description;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate releaseDate;

}
