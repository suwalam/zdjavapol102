package pl.sda.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ClassWithValue {

    @Value("${pl.sda.example}")
    private String injectedValue;

    public void printInjectedValue() {
        System.out.println("Injected value from application.properties: " + injectedValue);
    }

}
