package pl.sda.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.sda.model.Message;
import pl.sda.service.MessageService;

import java.util.List;

@Slf4j
//@Controller
//@ResponseBody//wymusza na Springu zamianę obiektu zwracanego przez metodę na format JSON
//@RestController oznacza odpowiednik dwóch powyższych adnotacji
@RestController
public class MessageController {

    @Autowired
    private MessageService messageService;

    @ResponseStatus(HttpStatus.OK) //200
    @GetMapping("/api/messages")
    public List<Message> getAll() {
      log.info("Returned all messages");
      return messageService.getAllMessages();
    }

    @ResponseStatus(HttpStatus.CREATED) //201
    @PostMapping("/api/messages")
    public void create(@RequestBody Message message) { //@RequestBody - zamiana JSON na obiekt Message
        log.info("Created new message: " + message);
        messageService.addMessage(message);
    }

    @ResponseStatus(HttpStatus.OK) //200
    @GetMapping("/api/messages/{id}")
    public Message getById(@PathVariable Integer id) { //@PathVariable - odczyt id z adresu URL i przypisanie do zmiennej id
        log.info("Returned message with id " + id);
        return messageService.getMessageById(id);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping("/api/messages")
    public void update(@RequestBody Message message) {
        log.info("Updated message: " + message);
        messageService.updateMessage(message);
    }

    @ResponseStatus(HttpStatus.OK)//200
    @DeleteMapping("/api/messages/{id}")
    public void delete(@PathVariable Integer id) {
        log.info("Deleted message with id " + id);
        messageService.deleteMessage(id);
    }

}
