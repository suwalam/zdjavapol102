package pl.sda.service;

import pl.sda.model.Book;

import java.util.List;

public interface BookService {

    void save(Book book);

    List<Book> getAll();

    void deleteByIsbn(String isbn);

    void update(Book book);

    Book getById(Integer id);

    Book getByIsbn(String isbn);

    List<Book> getAll(int pageNo, int pageSize, String sortBy);

}
