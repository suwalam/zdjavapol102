package pl.sda.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pl.sda.model.Book;
import pl.sda.repository.BookRepository;
import pl.sda.service.BookService;

import java.util.List;

@Slf4j
@Service
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;

    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public void save(Book book) {
        bookRepository.save(book);
    }

    @Override
    public List<Book> getAll() {
        return bookRepository.findAll();
    }

    @Override
    public void deleteByIsbn(String isbn) {
        Book book = getByIsbn(isbn);
        bookRepository.deleteById(book.getId());
    }

    @Override
    public void update(Book book) {
        bookRepository.save(book);
    }

    @Override
    public Book getById(Integer id) {
        return bookRepository.findById(id).orElse(null);
    }

    @Override
    public Book getByIsbn(String isbn) {
        return bookRepository.findByIsbn(isbn);
    }

    @Override
    public List<Book> getAll(int pageNo, int pageSize, String sortBy) {

        Pageable pageRequest = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

        Page<Book> bookPage = bookRepository.findAll(pageRequest);

        log.info("Total elements: " + bookPage.getTotalElements());
        log.info("Total pages: " + bookPage.getTotalPages());
        log.info("Elements in this page: " + bookPage.getNumberOfElements());
        log.info("Page number: " + bookPage.getNumber());

        return bookPage.getContent();
    }


}
