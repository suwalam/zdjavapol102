package pl.sda.controller;

import org.assertj.core.api.Assertions;
import org.hamcrest.Matchers;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import pl.sda.model.Book;
import pl.sda.repository.BookRepository;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@AutoConfigureMockMvc
@SpringBootTest
@RunWith(SpringRunner.class)
class BookControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private BookRepository bookRepository;

    //Na potrzeby testu zaloguj użytkownika o podanej roli
    @WithMockUser(username = "user", password = "pass", roles = "USER")
    @Test
    public void shouldReturnBookList() throws Exception {

        Book book = new Book();
        book.setId(1);
        book.setTitle("Python dla każdego. Podstawy programowania");
        book.setAuthor("Jan Kowalski");
        book.setIsbn("1234567891012");
        book.setDescription("Chcesz się nauczyć programować? Świetna decyzja! Wybierz język obiektowy, łatwy w użyciu, z przejrzystą składnią. Python będzie wprost doskonały! Rozwijany od ponad 20 lat, jest dojrzałym językiem, pozwalającym tworzyć zaawansowane aplikacje dla różnych systemów operacyjnych. Ponadto posiada system automatycznego zarządzania pamięcią, który zdejmuje z programisty obowiązek panowania nad tym skomplikowanym obszarem.");
        book.setReleaseDate(LocalDate.of(2014, 11, 11));

        mockMvc.perform(MockMvcRequestBuilders.get("/books/list"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("book-list"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("books"))
                .andExpect(MockMvcResultMatchers.model().attribute("books", Matchers.notNullValue()));
                //.andExpect(MockMvcResultMatchers.model().attribute("books", Matchers.contains(Arrays.asList(book))));
    }

    @WithMockUser(username = "admin", password = "pass", roles = "ADMIN")
    @Test
    public void shouldCreateBook() throws Exception {

        Book testBook = new Book();
        testBook.setId(4);
        testBook.setTitle("test");
        testBook.setAuthor("Testowy");
        testBook.setIsbn("1234567891017");
        testBook.setDescription("description");
        testBook.setReleaseDate(LocalDate.of(2014, 11, 11));

        mockMvc.perform(MockMvcRequestBuilders.post("/admin/books/save")
                .param("id", testBook.getId().toString())
                .param("title", testBook.getTitle())
                .param("author", testBook.getAuthor())
                .param("isbn", testBook.getIsbn())
                .param("description", testBook.getDescription())
                .param("releaseDate", testBook.getReleaseDate().toString())
        )//zakończenie metody perform
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.view().name("redirect:/books/list"))
                .andExpect(MockMvcResultMatchers.redirectedUrl("/books/list"));

        Optional<Book> optionalBook = bookRepository.findById(testBook.getId());

        Assertions.assertThat(optionalBook.isPresent()).isEqualTo(true);
        Book bookFromDB = optionalBook.get();
        Assertions.assertThat(bookFromDB).isEqualTo(testBook);


    }

}