package pl.sda.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.sda.dao.PersonDao;
import pl.sda.dao.impl.PersonDaoFileImpl;
import pl.sda.dao.impl.PersonDaoMockImpl;
import pl.sda.service.PersonService;
import pl.sda.service.impl.PersonServiceImpl;
import pl.sda.util.PersonUtil;

@Configuration
public class PersonConfiguration {

    @Bean
    public PersonUtil personUtil() {
        return new PersonUtil();
    }

    @Bean
    public PersonDao personDaoMock() {
        return new PersonDaoMockImpl();
    }

    @Bean
    public PersonDao personDaoFile() {
        return new PersonDaoFileImpl();
    }

    @Bean
    public PersonService personService() {
        PersonServiceImpl personService = new PersonServiceImpl();
        personService.setPersonUtil(personUtil());
        return personService;
    }
}
