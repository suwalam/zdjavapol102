package pl.sda.runner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.sda.cyclicdependency.ClassA;
import pl.sda.dependency.ClassWithDependency;
import pl.sda.primary.SimpleLogger;
import pl.sda.profile.ConnectionProperty;
import pl.sda.properties.ClassWithPropertiesCollection;
import pl.sda.properties.ClassWithValue;
import pl.sda.scope.RandomNumberReader1;
import pl.sda.scope.RandomNumberReader2;

@Component
public class ApplicationRunner implements CommandLineRunner {

    @Autowired
    private ClassWithDependency classWithDependency;

    @Autowired
    private ClassA classA;

    @Autowired
    private ClassWithValue classWithValue;

    @Autowired
    private ClassWithPropertiesCollection classWithPropertiesCollection;

    @Autowired
    private SimpleLogger logger;

    @Autowired
    private RandomNumberReader1 reader1;

    @Autowired
    private RandomNumberReader2 reader2;

    @Autowired
    private ConnectionProperty connectionProperty;

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Hello from Spring Boot");

        classWithDependency.printClasWithDependency();

        classWithValue.printInjectedValue();

        classWithPropertiesCollection.printUsers();

        classWithPropertiesCollection.printMap();

        classWithPropertiesCollection.printFieldA();

        logger.printMessage("message from ApplicationRunner");

        reader1.printRandomNumber();
        reader2.printRandomNumber();

        System.out.println(connectionProperty.getConnection());
    }
}
