package pl.sda.model;

//POJO - Plain Old Java Object

import lombok.*;


@Data //gettery, settery, toString, equals, hashcode
@AllArgsConstructor
@NoArgsConstructor
public class Person {

    private Integer id;

    private String name;

    private String surname;

}
