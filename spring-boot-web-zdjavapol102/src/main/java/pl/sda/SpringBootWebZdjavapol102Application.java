package pl.sda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootWebZdjavapol102Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootWebZdjavapol102Application.class, args);
	}

}
