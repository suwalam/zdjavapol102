package pl.sda.main;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import pl.sda.model.Message;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RestConsumer {

    public static void main(String[] args) throws JsonProcessingException {

        RestTemplate restTemplate = new RestTemplate();

        ObjectMapper objectMapper = new ObjectMapper();

       /* String getAllURL = "http://localhost:8081/api/messages";

        String jsonMessage = restTemplate.getForObject(getAllURL, String.class);

        System.out.println(jsonMessage);

        List<Message> messages = objectMapper.readValue(jsonMessage,
                objectMapper.getTypeFactory().constructCollectionType(List.class, Message.class));

        messages.forEach(System.out::println);*/

        //*****************************************************

        Integer id = 1;
        String getByIdURL = "http://localhost:8081/api/messages/{id}";
        Map<String, String> params = new HashMap<>();
        params.put("id", id.toString());

        Message messageById = restTemplate.getForObject(getByIdURL, Message.class, params);
        System.out.println(messageById);

        ResponseEntity<Message> responseEntity = restTemplate.getForEntity(getByIdURL, Message.class, params);

        System.out.println("Response status: " + responseEntity.getStatusCodeValue());
        System.out.println("Message from response entity: " + responseEntity.getBody());
        responseEntity
                .getHeaders()
                .entrySet()
                .stream()
                .forEach( e -> System.out.println("header name: " + e.getKey() + " header value: " + e.getValue()));
    }

}
