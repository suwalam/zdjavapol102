package pl.sda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootBookCatalogZdjavapol102Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootBookCatalogZdjavapol102Application.class, args);
	}

}
