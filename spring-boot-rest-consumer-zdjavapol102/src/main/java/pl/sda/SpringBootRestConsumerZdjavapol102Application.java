package pl.sda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRestConsumerZdjavapol102Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRestConsumerZdjavapol102Application.class, args);
	}

}
