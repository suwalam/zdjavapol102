package pl.sda.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import pl.sda.model.Order;
import pl.sda.service.OrderService;

@Slf4j
@Controller
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping("/orders/create")
    public String showOrderForm(ModelMap modelMap) {
        modelMap.addAttribute("emptyOrder", new Order());
        return "order-create";
    }

    @PostMapping("/orders/save")
    public String handleNewOrder(@ModelAttribute("emptyOrder") Order order) {
        log.info("Handle new order " + order);

        orderService.save(order);

        return "redirect:/orders/list";
    }

    @GetMapping("/orders/list")
    public String showOrders(ModelMap modelMap) {
        modelMap.addAttribute("orders", orderService.getAll());
        return "order-list";
    }

}
