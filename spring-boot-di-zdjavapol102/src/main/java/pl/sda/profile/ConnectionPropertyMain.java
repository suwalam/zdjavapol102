package pl.sda.profile;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile("!dev")
@Component
public class ConnectionPropertyMain implements ConnectionProperty {

    @Override
    public String getConnection() {
        return "connection MAIN";
    }
}
