package pl.sda.model;

//POJO - Plain Old Java Object

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data //gettery, settery, toString, equals, hashcode
@AllArgsConstructor
@NoArgsConstructor
public class Person {

    private Integer id;

    private String name;

    private String surname;

}
