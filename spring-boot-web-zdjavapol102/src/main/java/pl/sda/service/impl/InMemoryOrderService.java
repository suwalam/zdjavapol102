package pl.sda.service.impl;

import org.springframework.stereotype.Service;
import pl.sda.model.Order;
import pl.sda.service.OrderService;

import java.util.ArrayList;
import java.util.List;

@Service
public class InMemoryOrderService implements OrderService {

    private List<Order> orders;

    private static int counter;

    public InMemoryOrderService() {
        orders = new ArrayList<>();
        orders.add(new Order(1, "napoje", 30.4));
        orders.add(new Order(2, "książki", 230.67));
        counter = 2;
    }

    @Override
    public List<Order> getAll() {
        return orders;
    }

    @Override
    public void save(Order order) {
        order.setId(++counter);
        orders.add(order);
    }
}
