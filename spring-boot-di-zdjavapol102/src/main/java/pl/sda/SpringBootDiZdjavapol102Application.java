package pl.sda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import pl.sda.runner.ApplicationRunner;

@SpringBootApplication
public class SpringBootDiZdjavapol102Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootDiZdjavapol102Application.class, args);
	}
}
