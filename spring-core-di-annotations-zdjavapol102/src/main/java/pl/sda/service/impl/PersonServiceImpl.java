package pl.sda.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import pl.sda.dao.PersonDao;
import pl.sda.model.Person;
import pl.sda.service.PersonService;
import pl.sda.util.PersonUtil;

import java.util.List;

public class PersonServiceImpl implements PersonService {

    @Qualifier("personDaoMock")
    @Autowired
    private PersonDao personDao;

    private PersonUtil personUtil;

    @Override
    public Person getById(String id) {
        Integer parsedId = personUtil.parseStringToInteger(id);
        return personDao.getById(parsedId);
    }

    @Override
    public List<Person> getAll() {
        return personDao.getAll();
    }

    public void setPersonUtil(PersonUtil personUtil) {
        this.personUtil = personUtil;
    }
}
