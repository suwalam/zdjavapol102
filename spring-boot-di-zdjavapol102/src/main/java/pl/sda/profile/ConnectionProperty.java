package pl.sda.profile;

public interface ConnectionProperty {

    String getConnection();

}
