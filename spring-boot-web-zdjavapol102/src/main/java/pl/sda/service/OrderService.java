package pl.sda.service;

import pl.sda.model.Order;

import java.util.List;

public interface OrderService {

    List<Order> getAll();

    void save(Order order);

}
