package pl.sda.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import pl.sda.model.Book;
import pl.sda.service.BookService;

import javax.validation.Valid;

@Slf4j
@Controller
public class BookController {

    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/books/list")
    public String bookList(ModelMap modelMap) {
        modelMap.addAttribute("books", bookService.getAll());

        String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        modelMap.addAttribute("currentUser", currentUser);

        return "book-list";
    }

    @GetMapping("/admin/books/create")
    public String showCreateBookForm(ModelMap modelMap) {
        modelMap.addAttribute("emptyBook", new Book());
        return "book-create";
    }

    @PostMapping("/admin/books/save")
    public String handleNewBook(@Valid @ModelAttribute("emptyBook") Book book, Errors errors) {
        log.info("Handle new book: " + book);

        if (errors.hasErrors()) {
            log.error("Errors form frontend: " + errors.getFieldErrors());
            return "book-create";
        }

        bookService.save(book);
        return "redirect:/books/list";
    }

    @GetMapping("/books/{isbn}")
    public String bookDetails(@PathVariable String isbn, ModelMap modelMap) {
        modelMap.addAttribute("book", bookService.getByIsbn(isbn));
        return "book-details";
    }

    @GetMapping("/admin/books/edit/{isbn}")
    public String showEditBookForm(@PathVariable String isbn, ModelMap modelMap) {
        modelMap.addAttribute("book", bookService.getByIsbn(isbn));
        return "book-edit";
    }

    @PostMapping("/admin/books/update")
    public String handleUpdatedBook(@Valid @ModelAttribute("book") Book book, Errors errors) {
        log.info("Handle book to update: " + book);

        if (errors.hasErrors()) {
            log.error("Errors form frontend: " + errors.getFieldErrors());
            return "book-edit";
        }

        bookService.update(book);
        return "redirect:/books/list";
    }

    @GetMapping("/admin/books/delete/{isbn}")
    public String deleteBook(@PathVariable String isbn) {
        log.info("Deleted book with isbn " + isbn);
        bookService.deleteByIsbn(isbn);
        return "redirect:/books/list";
    }

    @GetMapping("/books/list/params")
    public String bookListParametrized(ModelMap modelMap,
                                       @RequestParam(defaultValue = "0") Integer pageNo,
                                       @RequestParam(defaultValue = "2") Integer pageSize,
                                       @RequestParam(defaultValue = "id") String sortBy) {

        modelMap.addAttribute("books", bookService.getAll(pageNo, pageSize, sortBy));
        return "book-list";
    }


}
