package pl.sda.main;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class BCryptPasswordEncoderMain {

    public static void main(String[] args) {

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        System.out.println(encoder.encode("hasło"));//zamienia surowe hasło na postać zahashowaną

        System.out.println(encoder.matches("hasło", "$2a$10$8XkkydzyP5/TINMlGR80cu3tbDDLVRAGl/JWp8EFkEE4K3ksA.Y.6"));

        validatePassword();
    }

    public static void validatePassword() {
        String passwd = "";
        String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";
        System.out.println(passwd.matches(pattern));
    }


}
